﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace ShortestPath
{
    public class Node
    {
        public string Name;
        public List<Arc> Arcs = new List<Arc>();

        public Node(string name)
        {
            Name = name;
        }

        
        public Node AddArc(Node child, int w)
        {
            Arcs.Add(new Arc
            {
                Parent = this,
                Child = child,
                Weigth = w
            });
            if (!child.Arcs.Exists(a => a.Parent == child && a.Child == this))
            {
                child.AddArc(this, w);
            }

            return this;
        }
    }


    public class Arc
    {
        public int Weigth;
        public Node Parent;
        public Node Child;
    }
    public class Graph
    {
        public Node Root;
        public List<Node> AllNodes = new List<Node>();

        public Node CreateRoot(string name)
        {
            Root = CreateNode(name);
            return Root;
        }

        public Node CreateNode(string name)
        {
            var n = new Node(name);
            AllNodes.Add(n);
            return n;
        }
        public int[,] CreateAdjMatrix()
        {
            int[,] adj = new int[AllNodes.Count, AllNodes.Count];
            for (int i = 0; i < AllNodes.Count; i++)
            {
                Node n1 = AllNodes[i];
                for (int j = 0; j < AllNodes.Count; j++)
                {
                    Node n2 = AllNodes[j];
                    var arc = n1.Arcs.FirstOrDefault(a => a.Child == n2);
                    if (arc != null)
                    {
                        adj[i, j] = arc.Weigth;
                    }
                }
            }
            return adj;
        }
        public static void PrintMatrix(ref int[,] matrix, int Count)
        {
            Console.Write("        ");
            for (int i = 0; i<Count; i++)
            {
                if (i == 6)
                    Console.Write("{0} ", "12");
                else if (i == 7)
                    Console.Write("{0} ", "13");
                else if (i == 9)
                    Console.Write("{0}  ", "10");
                else
                    Console.Write("{0}  ",(char) ('1' + i));
            }
            Console.WriteLine();
            for (int i = 0; i < Count; i++)
            {
                if (i == 6)
                    Console.Write("{0} [  ", "12");
                else if (i == 7)
                    Console.Write("{0} [  ", "13");
                else if(i==9)
                    Console.Write("{0} [  ", "10");
                else
                    Console.Write("{0}  [  ", (char)('1' + i));
                for (int j = 0; j < Count; j++)
                {
                    if (matrix[i, j] == 0)
                    {
                        Console.Write(" .,");
                    }
                    else
                    {
                        Console.Write(" {0},", matrix[i, j]);
                    }

                }
                Console.Write(" ]\r\n");
            }
            Console.Write("\r\n");
            Console.ReadLine();
        }

    }
   class Program
    {
        private static int MinimumDistance(int[] distance, bool[] shortestPathTreeSet, int verticesCount)
        {
            int min = int.MaxValue;
            int minIndex = 0;
            for (int v = 0; v < verticesCount; ++v)
            {
                if (shortestPathTreeSet[v] == false && distance[v] <= min)
                {
                    min = distance[v];
                    minIndex = v;
                }
            }
           return minIndex;
        }
        private static void Print(int[] distance, int verticesCount, int desti)
        {
            Console.WriteLine("Shortest Distance from source to destination is=");
            for (int i = 0; i < verticesCount; ++i)
                if(desti==i)
                Console.WriteLine(distance[i]);
            Console.ReadLine();
        }
        public static void Dijkstra(int[,] graph, int source, int verticesCount, int destination)
        {
            int[] distance = new int[verticesCount];
            bool[] shortestPathTreeSet = new bool[verticesCount];
            int[] parent = new int[verticesCount];
            for (int i = 0; i < verticesCount; ++i)
            {
                distance[i] = int.MaxValue;
                shortestPathTreeSet[i] = false;
            }
            distance[source] = 0;
            for (int count = 0; count < verticesCount - 1; ++count)
            {
                int u = MinimumDistance(distance, shortestPathTreeSet, verticesCount);
                shortestPathTreeSet[u] = true;
                for (int v = 0; v < verticesCount; ++v)
                {
                    if (!shortestPathTreeSet[v] && Convert.ToBoolean(graph[u, v]) && distance[u] != int.MaxValue && distance[u] + graph[u, v] < distance[v])
                    {
                        distance[v] = distance[u] + graph[u, v];
                        parent[v] = u;
                    }
                }
                
            }
            List<int> shortestPath = new List<int>();
            int current = destination;
            while (current != source)
            {
                shortestPath.Add(current);
                current = parent[current];
            }
            shortestPath.Add(source);
            shortestPath.Reverse();
            for(int i=0;i<shortestPath.Count;i++)
            {
                if (shortestPath[i] == 6)
                    shortestPath[i] = 12;
                else if (shortestPath[i] == 7)
                    shortestPath[i] = 13;
                else
                    shortestPath[i] = shortestPath[i] + 1;
            }
            Console.WriteLine("Shortest Path");
            Console.WriteLine(string.Join("-->", shortestPath));
            Console.ReadLine();
            Print(distance, verticesCount, destination);
       }
       static void Main(string[] args)
        {
            Graph gr= new Graph();
            var a = gr.CreateRoot("1");
            var b = gr.CreateNode("2");
            var c = gr.CreateNode("3");
            var d = gr.CreateNode("4");
            var e = gr.CreateNode("5");
            var f = gr.CreateNode("6");
            var g = gr.CreateNode("9");
            var h = gr.CreateNode("10");
            var i = gr.CreateNode("12");
            var j = gr.CreateNode("13");


            a.AddArc(b, 1)
             .AddArc(c, 2);

            b.AddArc(a, 1)
             .AddArc(e, 3);

            c.AddArc(a, 2)
             .AddArc(d, 1);

            d.AddArc(c, 1)
             .AddArc(f, 2)
             .AddArc(g, 8);

            e.AddArc(b, 3)
             .AddArc(f, 1)
             .AddArc(i, 4);

            f.AddArc(e, 1)
             .AddArc(d, 2);

            g.AddArc(d, 8)
             .AddArc(h, 2)
             .AddArc(i, 3);

            h.AddArc(g, 2)
             .AddArc(j, 3);


            i.AddArc(g, 3)
             .AddArc(j, 1);

            j.AddArc(i, 1)
             .AddArc(h, 3);

            int[,] graph = gr.CreateAdjMatrix(); 
            Graph.PrintMatrix(ref graph, gr.AllNodes.Count); 
            Console.WriteLine("Enter the source vertex");
            int source = Convert.ToInt16(Console.ReadLine());
            Console.WriteLine("Enter the destination vertex");
            int destination= Convert.ToInt16(Console.ReadLine());
            if (source == destination)
                Console.WriteLine("Source and Destination are same");
            else if ((source == 8) || (source == 7) || (destination == 8) || (destination == 7)|| (source<=0)|| (destination<=0))
            {
                Console.WriteLine("No such Vertex exist in following graph");
                Console.ReadLine();
                Environment.Exit(0);
            }
            Console.ReadLine();
            if (source == 12)
                source = 6;
            else if (source == 13)
                source = 7;
            else
                source = source - 1;

            if (destination == 12)
                destination = 6;
            else if (destination == 13)
                destination = 7;
            else
                destination = destination - 1;
            Dijkstra(graph, source, 10, destination);
        }
    }
}
