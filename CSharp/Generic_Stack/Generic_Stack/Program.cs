﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_Stack
{
    class MyStack<T>
    {
        private int _capacity;
        private T[]_stack;
        private int _top;

        public MyStack(int maxElements)
        {
            _capacity = maxElements;
            _stack = new T[_capacity];
            _top = -1;
        }
        public int Push(T element)
        {
            // Check overflow
            if (_top == _capacity-1)
            {
                // Return -1 if over flow is there
                return -1;
            }
            else
            {
                // Insert elementt into stack
                _top = _top+1;
                _stack[_top] = element;
            }
            return 0;
        }
        public T Pop()
        {
            T removedElement;
            T temp = default(T);
            // Check Underflow
            if (_top>-1)
            {
                removedElement = _stack[_top];
                _stack[_top] = default(T);
                _top = _top-1;
                return removedElement;
            }
            return temp;
        }
        public T Peep(string position)
        {
            T temp = default(T);
            bool check=int.TryParse(position, out int value);
            // Check if Position is Valid or not
            if (check == true)
            {
                if ((value < _capacity && value >= 0)&& _stack[value]!=null)
                {
                    return _stack[value];
                }
                else
                {
                    Console.WriteLine("Element is out of stack range!!!!");
                }
            }
            else
            {
                Console.WriteLine("You have entered wrong position!!");
            }
            return temp;
        }
        public T[] GetAllStackElements()
        {
            T[] elements = new T[_top + 1];
            Array.Copy(_stack, 0, elements, 0, _top + 1);
            return elements;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int capacity;
            Console.Write("Enter Capacity of Stack :");
            capacity = int.Parse(Console.ReadLine());
            MyStack<string> stack = new MyStack<string>(capacity);
            while (true)
            {
                Console.WriteLine("--------------------Menu------------------------");
                Console.WriteLine("1.Push");
                Console.WriteLine("2.Pop");
                Console.WriteLine("3.Peep");
                Console.WriteLine("4.Print Stack Elements:");
                Console.WriteLine("5.Exit");
                Console.Write("Enter your Choice :");
                var choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                    {
                        Console.Write("Enter String to Push :");
                        string temp = Console.ReadLine();
                        int result = stack.Push(temp);
                        if (result != -1)
                        {
                            Console.WriteLine("Element Pushed into Stack!!!!!!");
                        }
                        else
                        {
                            Console.WriteLine("Stack Overflow!!!!!");
                        }
                        break;
                    }
                    case "2":
                    {
                        string result = stack.Pop();
                        if (result != null)
                        {
                            Console.WriteLine("Deleted Element :" + result);
                        }
                        else
                        {
                            Console.WriteLine("Stack Underflow!!!!!");
                        }
                        break;
                    }
                    case "3":
                    {
                        Console.Write("Enter Position of Element to Pop:");
                        var position = Console.ReadLine();
                        string result = stack.Peep(position);
                        if (result != null)
                        {
                            Console.WriteLine("Element at Position" + " "+position + " is " + result);
                        }
                        else
                        {
                            
                        }
                        break;
                    }
                    case "4":
                    {
                        string []elements=stack.GetAllStackElements();
                        Console.WriteLine("**************Stack Content **************");
                        if(elements.Length==0)
                        {
                            Console.WriteLine("The Stack is empty");
                        }
                        foreach (string str in elements)
                        { 
                            Console.WriteLine(str);
                        }
                        break;
                    }
                    case "5":
                    {
                        Environment.Exit(0);
                        break;
                    }
                    default:
                    {
                        Console.WriteLine("You have Entered Wrong Choice ");
                        break;
                    }
                }
            }
        }
    }
}

