﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_Queue
{
    public class MyQueue <T>
    {
        private T[] _data;
        private int _front;
        private int _rear;
        private int _capacity;

        public MyQueue(int maxElements)
        {
            _capacity = maxElements;
            _data = new T[_capacity];
            _front = _rear = -1;
        }

        public bool IsEmptyQueue()
        {
            if(_front == -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsFullQueue()
        {
            if((_rear+1)%_capacity == _front)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int QueueSize()
        {
            return(((_capacity-_front)+_rear+1)%_capacity);
        }

        public int EnQueue(T element)
        {
            if(IsFullQueue())
            {
                return -1;
            }
            else
            {
                _rear = (_rear + 1) % _capacity;
                _data[_rear] = element;
                if(_front == -1)
                {
                    _front = _rear;
                }
            }
            return 1;
        }

        public T DeQueue()
        {
            T removedElement;
            T temp = default(T);
            if(IsEmptyQueue())
            {
                Console.WriteLine("Queue is Empty");
                return temp;
            }
            else
            {
                removedElement = _data[_front];
                if(_front == _rear)
                {
                    _front = _rear = -1;
                }
                else
                {
                    _front = (_front + 1) % _capacity;
                }
            }
            return removedElement;
        }

       /* public T Peek()
        {
            if(_front > _rear)
            {
                return _data[_]
            }
        }*/

        public T[] GetAllQueueElements()
        {
            T[] elements = new T[QueueSize()];
            if (_front < _rear)
            {
                Array.Copy(_data, _front, elements, 0 , QueueSize());
            }
            else
            {
                Array.Copy(_data, _rear, elements, 0 , QueueSize());
            }
            return elements;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int capacity;
            Console.WriteLine("Enter Capacity of Queue :");
            capacity = int.Parse(Console.ReadLine());
            MyQueue<string> queue = new MyQueue<string>(capacity);
            while (true)
            {
                Console.WriteLine("Select your choice");
                Console.WriteLine("1.EnQueue");
                Console.WriteLine("2.DeQueue");
                /*Console.WriteLine("3.Peep");*/
                Console.WriteLine("4.Print Queue Elements:");
                Console.WriteLine("5.Exit");
                Console.WriteLine("Enter your Choice :");
                int choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        {
                            Console.WriteLine("Enter String to insert :");
                            string temp = Console.ReadLine();
                            int result = queue.EnQueue(temp);
                            if (result != -1)
                            {
                                Console.WriteLine("Element inserted into Queue !");
                            }
                            else
                            {
                                Console.WriteLine("Stack Overflow !");
                            }
                            break;
                        }
                    case 2:
                        {
                            string result = queue.DeQueue();
                            if (result != null)
                            {
                                Console.WriteLine("Deleted Element :" + result);
                            }
                            else
                            {
                                Console.WriteLine("Stack Underflow !");
                            }
                            break;
                        }
                   /* case 3:
                        {
                            Console.WriteLine("Enter Position of Element to Pop:");
                            int position = int.Parse(Console.ReadLine());
                            string result = stack.Peep(position);
                            if (result != null)
                            {
                                Console.WriteLine("Element at Position" + position + " is " + result);
                            }
                            else
                            {
                                Console.WriteLine("Entered Element is Out of Stack Range ");
                            }
                            break;
                        }*/
                    case 4:
                        {
                            string[] elements = queue.GetAllQueueElements();
                            Console.WriteLine("**************Queue Content **************");
                            foreach (string str in elements)
                            {
                                Console.WriteLine(str);
                            }
                            break;
                        }
                    case 5:
                        {
                            System.Environment.Exit(0);
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("You have Entered Wrong Choice ");
                            break;
                        }
                }
                Console.ReadLine();
            }
        }
    }
}

