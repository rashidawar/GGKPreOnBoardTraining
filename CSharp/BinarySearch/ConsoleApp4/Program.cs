﻿using System;
using System.Collections.Generic;
namespace BinarySearch
{
    public class BinarySearchTree<T> where T : IComparable<T>
    {
        public class BinaryNode<T> where T : IComparable<T>
        {
                public T data;
                //public BinaryNode<T> parent;
                public BinaryNode<T> leftChild;
                public BinaryNode<T> rightChild;
                public BinaryNode()
                {
                    return;
                }
                public BinaryNode(T value)
                {
                    data = value;
                }

        }
        public BinaryNode<T> root;
        public BinarySearchTree()
        {
           root = null;
        }
            // Insertion
         public void Insert(T item)
         {
            BinaryNode<T> newNode = new BinaryNode<T>(item);
            int compare = 0;
            if (root == null)
            {
                root = newNode;
            }
            else
            {
                BinaryNode<T> current = root;
                BinaryNode<T> parent;
                while (true)
                {
                    parent = current;
                    compare = current.data.CompareTo(item);
                    if (compare > 0)
                    {
                        current = current.leftChild;
                        if (current == null)
                        {
                            parent.leftChild = newNode;
                            return;
                        }
                    }
                    else
                    {
                        current = current.rightChild;
                        if (current == null)
                        {
                            parent.rightChild = newNode;
                            return;
                        }
                    }
                }
            }
         }
            // Find
         public BinaryNode<T> Find(BinaryNode<T> root, T item)
         {
            if (root == null)
            {
                return (null);
            }
            int compare = 0;
            compare = root.data.CompareTo(item);
            if (compare > 0)
            {
                return (Find(root.leftChild, item));
            }
            else if (compare < 0)
            {
                return (Find(root.rightChild, item));
            }
            return root;
         }

         
        // Inorder traversal
        public void Display(BinaryNode<T> root)
         {
            if (root != null)
            {
                Display(root.leftChild);
                Console.WriteLine(root.data);
                Display(root.rightChild);
            }

        }

    }
    class Program
    { 
        public static void Main(string[] args)
        {
            string input;
            BinarySearchTree<string> bst = new BinarySearchTree<string>();
            BinarySearchTree<string>.BinaryNode<string>temp;
            while (true)
            {
                Console.WriteLine("------------------------Menu---------------------");
                Console.WriteLine("1. Insert in the Tree");
                Console.WriteLine("2. Find the element in the Tree");
                Console.WriteLine("3. Print the Tree");
                Console.WriteLine("4. Exit");
                Console.Write("Enter your choice=");
                var choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        Console.Write("Enter the element you want to insert=");
                        input = Console.ReadLine();
                        bst.Insert(input);
                        Console.WriteLine("Element entered");
                        break;
                    case "2":
                        Console.Write("Enter the element you want to find=");
                        input = Console.ReadLine();
                        temp=bst.Find(bst.root, input);
                        if (temp != null)
                        {
                            Console.WriteLine("The element found");
                        }
                        else
                        {
                            Console.WriteLine("The element could not be found");
                        }
                        break;
                    case "3":
                        if (bst.root == null)
                        {
                            Console.WriteLine("Tree is empty");
                        }
                        else
                        {
                            bst.Display(bst.root);
                        }
                        break;
                    case "4":
                        Console.WriteLine("Thanks");
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("You have entered wrong choice");
                        break;
                           
                }
            }
        }
    }
}

