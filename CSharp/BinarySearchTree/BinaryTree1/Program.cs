﻿using System;
namespace BinarySearchTree
{
    class Node
    {
        public int item;
        public Node leftc;
        public Node rightc;
    }
    class Tree
    {
        public Node root;
        public Tree()
        {
            root = null;
        }
        public Node ReturnRoot()
        {
            return root;
        }
        public void Insert(int id)
        {
            Node newNode = new Node();
            newNode.item = id;
            if (root == null)
                root = newNode;
            else
            {
                Node current = root;
                Node parent;
                while (true)
                {
                    parent = current;
                    if (id < current.item)
                    {
                        current = current.leftc;
                        if (current == null)
                        {
                            parent.leftc = newNode;
                            return;
                        }
                    }
                    else
                    {
                        current = current.rightc;
                        if (current == null)
                        {
                            parent.rightc = newNode;
                            return;
                        }
                    }
                }
            }
        }
        public Node Find(Node Root, int data)
        {
            if (Root == null)
                return (null);
            if (data < Root.item)
                return (Find(Root.leftc, data));
            else if(data>Root.item)
                return (Find(Root.rightc, data));
            return (Root);
        } 
        public  int[] Ascending(Node Root, int size)
        {
            int[] arr = new int[size];
            int i=-1;
            Node current, pre;

            if (Root == null)
                return null;

            current = Root;
            while (current != null)
            {
                if (current.leftc == null)
                {
                    arr[++i]=Convert.ToInt32(current.item);
                   current = current.rightc;
                }
                else
                {
                    //Find the inorder predecessor of current
                    pre = current.leftc;
                    while (pre.rightc != null && pre.rightc != current)
                        pre = pre.rightc;

                    // Make current as right child of its inorder predecessor
                    if (pre.rightc == null)
                    {
                        pre.rightc = current;
                        current = current.leftc;
                    }

                    // Revert the changes made in if part to restore the original 
                      //tree i.e., fix the right child of predecssor
                    else
                    {
                        pre.rightc = null;
                        arr[++i] = Convert.ToInt32(current.item);
                        
                        current = current.rightc;
                    }
                }
            }
            return arr;

        }
        public void Descending(int[] arrayDesc)
        {
            Array.Reverse(arrayDesc);
            Console.WriteLine(string.Join(",", arrayDesc));
        }
 }
    class Program
    {
        static void Main(string[] args)
        {
            Tree theTree = new Tree();
          
            Console.WriteLine("Enter the total number of nodes you want=");
            int totalNodes = Convert.ToInt32(Console.ReadLine());
            int[] array = new int[totalNodes];
            int value;
            for (int i = 1; i <=totalNodes; i++)
            {
                Console.WriteLine("The {0} value to enter is", i);
                value = Convert.ToInt32(Console.ReadLine());
                theTree.Insert(value);
            }
            Console.WriteLine("1.Elements in ascending order");
            array=theTree.Ascending(theTree.ReturnRoot(), totalNodes);
            Console.WriteLine(string.Join(",", array));
            Console.ReadLine();

            Console.WriteLine("2.Elements in descending order");
            theTree.Descending(array);
            Console.ReadLine();

            Console.WriteLine("3.Search an element");
            Console.WriteLine("Enter the number you want to find=");
            int num_find = Convert.ToInt32(Console.ReadLine());
            Node find = theTree.Find(theTree.ReturnRoot(), num_find);
            if (find == null)
            {
                Console.WriteLine("No such element found");
                Console.WriteLine(" ");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("element found");
                Console.WriteLine(" ");
                Console.ReadLine();
            }
         }
    }
}
