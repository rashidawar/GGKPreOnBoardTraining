﻿using System;

namespace FactoryDesignPattern
{
    abstract class IVehicle
    {
        public abstract void Build();

    }
    class Bus : IVehicle
    {
        public override void Build()
        {
            Console.WriteLine("Bus has been created");
        }
    }
    class Car : IVehicle
    {
        public override void Build()
        {
            Console.WriteLine("Car has been created");
        }
    }
    class Truck : IVehicle
    {
        public override void Build()
        {
            Console.WriteLine("Truck has been created");
        }
    }

    class Factory
    {
        static public void BuildVehicles()
        {
            string choice;

            IVehicle vehicle;
            while (true)
            {
                Console.WriteLine("----------------Menu----------------");
                Console.WriteLine("Bus");
                Console.WriteLine("Car");
                Console.WriteLine("Truck");
                Console.WriteLine("Exit");
                Console.WriteLine("Enter your choice:");
                choice = Console.ReadLine();
                choice = choice.ToLower();
                switch (choice)
                {
                    case "bus":
                        vehicle = new Bus();
                        vehicle.Build();
                        break;
                    case "car":
                        vehicle = new Car();
                        vehicle.Build();
                        break;
                    case "truck":
                        vehicle = new Truck();
                        vehicle.Build();
                        break;
                    case "exit":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("You have entered a wrong choice");
                        break;
                }
            }

        }
        static void Main(string[] args)
        {
            BuildVehicles();
            Console.ReadKey();
        }
    }
}
