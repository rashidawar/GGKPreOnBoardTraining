﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_Queue
{
    public class MyQueue<T>
    {
        private T[] _data;
        private int _front;
        private int _rear;
        private int _capacity;

        public MyQueue(int maxElements)
        {
            _capacity = maxElements;
            _data = new T[_capacity];
            _front = _rear = -1;
        }

        public bool IsEmptyQueue()
        {
            if (_front == -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsFullQueue()
        {
            if ((_front == 0 && _rear == _data.Length - 1) || (_front == _rear + 1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int EnQueue(T element)
        {
            if (IsFullQueue())
            {
                return -1;
            }
            else
            {
                if(_rear == _data.Length - 1)
                {
                    _rear = 0;
                }
                else
                {
                    _rear++;
                }
                Console.WriteLine("Number is Pushed");
                _data[_rear] = element;
                
                if (_front == -1)
                {
                    _front++;
                }
            }
            return 1;
        }

        public void DeQueue()
        {
            
            
            if (IsEmptyQueue())
            {
                Console.WriteLine("Queue is Empty");
                Console.WriteLine("Queue UnderFlow!!!");
            }
            else
            {
                _data[_front] = default(T);
                Console.WriteLine("Number is Popped");
                if (_front == _rear)
                {
                    _front = -1;
                    _rear= -1;
                }
                else
                {
                    _front++;
                }
            }
            
        }

        public void GetAllQueueElements()
        {
            Console.WriteLine("**************Queue Content**************");
            foreach (T t in _data)
            {
                Console.WriteLine(t + " ");
            }
            
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int capacity;
            Console.Write("Enter Capacity of Queue :");
            capacity = int.Parse(Console.ReadLine());
            MyQueue<string> queue = new MyQueue<string>(capacity);
            while (true)
            {
                Console.WriteLine("---------------------Menu-------------------");
                Console.WriteLine("1.EnQueue");
                Console.WriteLine("2.DeQueue");
                Console.WriteLine("3.Print Queue Elements:");
                Console.WriteLine("4.Exit");
                Console.Write("Enter your Choice :");
                var choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        {
                            Console.Write("Enter String to insert :");
                            string temp = Console.ReadLine();
                            int result = queue.EnQueue(temp);
                            if (result != -1)
                            {
                                Console.WriteLine("Element inserted into Queue!!!");
                            }
                            else
                            {
                                Console.WriteLine("Queue Overflow!!!!");
                            }
                            break;
                        }
                    case "2":
                        {
                            queue.DeQueue();
                            break;
                        }
                    case "3":
                        {
                            queue.GetAllQueueElements();
                            break;
                        }
                    case "4":
                        {
                            Environment.Exit(0);
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("You have Entered Wrong Choice ");
                            break;
                        }
                }
            }
        }
    }
}

