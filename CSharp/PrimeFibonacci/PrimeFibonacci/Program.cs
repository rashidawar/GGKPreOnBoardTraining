﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace PrimeFibonacci
{
    class PrimeFibonacci
    {
        static int PrimeNum(int num)
        {
            int k;
            k = 0;
            for (int i = 1; i <= num; i++)
            {
                if (num % i == 0)
                    k++;
            }
            if (k == 2)
               return num;
            else
                return 0;
        }
        static void Main(string[] args)
        {
            Console.Write("Input=");
            int n =Convert.ToInt32(Console.ReadLine());
            int count = 0;
            int first = 0;
            int second = 1;
            int sum;
            List<int> prime = new List<int>();
            while (count <=n)
            {
                sum = first + second;
                // To check whether the sum obtained is a prime number
                int number=PrimeNum(sum);
                if (number != 0)
                    prime.Add(sum);
                first = second;
                second = sum;
                count++;
            }
            for(int i=0;i<prime.Count;i++)
            {
                for(int j=0;j<=i;j++)
                {
                    Console.Write("\t"+prime[j].ToString());
                }
                Console.Write("\n");
            }
            Console.ReadLine();

        }
    }
}
