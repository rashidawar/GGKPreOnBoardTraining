﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloatMultiply
{
    class FloatMultiply
    {
        // To add two numbers while multiplication 
        static String GetSum(String[] number1, String[] number2)
        {
            int carry = 0, value = 0, i = 0;
            String item = string.Empty;
            String binary1 = String.Join("", number1);
            String binary2 = String.Join("", number2);
            int len1 = binary1.Length;
            int len2 = binary2.Length;
            if (len1 > len2)
            {
                value = len1;
                binary2 = item.PadLeft(len1 - len2, '0') + binary2;
            }
            else if (len2 > len1)
            {
                value = len2;
                binary1 = item.PadLeft(len2 - len1, '0') + binary1;
            }
            else
                value = len1;
            byte[] bin1 = Encoding.ASCII.GetBytes(binary1);
            byte[] bin2 = Encoding.ASCII.GetBytes(binary2);
            int[] result = new int[value + 1];
            int count = value;
            for (i = value - 1; i >= 0; i--)
            {
                if ((bin1[i] - 48) + (bin2[i] - 48) + carry == 3)
                {
                    result[count] = 1;
                    count--;
                    carry = 1;
                }
                else if ((bin1[i] - 48) + (bin2[i] - 48) + carry == 2)
                {
                    result[count] = 0;
                    count--;
                    carry = 1;
                }
                else if ((bin1[i] - 48) + (bin2[i] - 48) + carry == 1)
                {
                    result[count] = 1;
                    count--;
                    carry = 0;
                }
                else if ((bin1[i] - 48) + (bin2[i] - 48) + carry == 0)
                {
                    result[count] = 0;
                    count--;
                    carry = 0;
                }
            }
            result[0] = carry;
            return String.Join("", result);
        }

        // Multiplication of Two binary numbers
        static String GetBinaryMultiply(String num1, String num2)
        {
            String[] number1 = num1.ToCharArray().Select(c => c.ToString()).ToArray();
            String[] number2 = num2.ToCharArray().Select(c => c.ToString()).ToArray();
            String[] prev = new String[num1.Length];
            int k = num1.Length;
            String[] result = new String[k];
            int count = 1, num = 1, tp = 1;
            String value;
            String temp = string.Empty;
            String temp1;
            String item = string.Empty;
            k = k - 1;
            for (int i = num2.Length - 1; i >= 0; i--)
            {
                for (int j = num1.Length - 1; j >= 0; j--)
                {
                    result[k] = (int.Parse(number1[j]) * int.Parse(number2[i])).ToString();
                    k = k - 1;
                }
                item = String.Join("", result);
                if (tp == 1)
                {
                    value = String.Join("", result);
                    tp--;
                }
                else
                {
                    temp1 = String.Empty;
                    temp1 = item + temp1.PadRight(count, '0');
                    result = temp1.ToCharArray().Select(c => c.ToString()).ToArray();
                    value = GetSum(result, prev);
                    count++;
                }

                prev = value.ToCharArray().Select(c => c.ToString()).ToArray();
                Array.Clear(result, 0, result.Length);
                num = num + 1;
                Array.Resize(ref result, result.Length + 1);
                k = result.Length - num;
            }
            String answer = String.Join("", prev);
            return answer;
        }

        // To convert the float value to decimal
        static String ToBinaryString(float value, int sign)
        {
            if (sign == 1)
                value = float.Parse(value.ToString().TrimStart('-'));
            long integral;
            long binaryInt = 0;
            long i = 1;
            decimal binaryFract = 0;
            decimal fractional;
            decimal binaryTotal;
            decimal k = 0.1m, temp1 = 0;
            String result;
            decimal value1 = decimal.Parse(value.ToString());
            integral = (int)value1;
            fractional = value1 - (int)value1;

            while (integral > 0)
            {
                binaryInt = binaryInt + integral % 2 * i;
                i = i * 10;
                integral = integral / 2;

            }

            while ((k > 0.000001m))
            {
                temp1 = fractional * 2;
                binaryFract = binaryFract + ((int)temp1) * k;
                fractional = temp1 - (int)temp1;
                k = k / 10;
            }
            binaryTotal = binaryInt + binaryFract;
            if (sign == 1)
                result = "1" + (binaryTotal.ToString());
            else
                result = "0" + binaryTotal.ToString();
            return result;
        }

        // To convert binary to decimal for integral part
        static String GetBinToDeci(String[] binary)
        {
            String sum = String.Empty;
            double lastDigit = 0;
            double value = 0;
            int strn = int.Parse((String.Join("", binary).Length).ToString());
            int count = 0;
            for (int i = strn - 1; i >= 0; i--)
            {
                // Get the last digit
                lastDigit = double.Parse(binary[i]);
                value = value + lastDigit * (Math.Pow(2, count));
                count++;
            }
            sum = value.ToString();
            return sum;
        }

        // To convert binary to decimal for fractional part
        static String GetBinToDeciFrac(String[] data)
        {
            decimal fracDecimal = 0;
            int count = -1;
            int length = int.Parse((String.Join("", data).Length).ToString());
            for (int i = 0; i < length; i++)
            {
                fracDecimal = fracDecimal + (decimal.Parse(data[i])) * decimal.Parse(Math.Pow(2, count).ToString());
                count--;
            }
            return fracDecimal.ToString();
        }

        // To count the length of digits after decimal
        static int GetDeciCount(String data)
        {
            String[] binary = data.ToCharArray().Select(c => c.ToString()).ToArray();
            int length = int.Parse((String.Join("", data).Length).ToString());
            int count = 0, i = length - 1;
            while (binary[i] != ".")
            {
                count++;
                i--;
            }
            return count;
        }

        // To determine the sign of the number
        static char DetermineSign(int value)
        {
            if (value == 1)
                return '-';
            else
                return '+';
        }

        static void Main(string[] args)
        {
            Console.Write("Enter the first number=");
            String num1 = Console.ReadLine();
            float data1, n, m;
            int neg1 = 0, neg2 = 0, sign;
            String binary1 = String.Empty;
            String binary2 = String.Empty;
            if (float.TryParse(num1, out data1))
                n = float.Parse(num1);
            else
            {
                Console.WriteLine("Wrong Input. Please re-Enter the number");
                Console.ReadLine();
                n = 0;
                Environment.Exit(0);
            }

            Console.Write("Enter the second number=");
            String num2 = Console.ReadLine();
            if (float.TryParse(num2, out data1))
                m = float.Parse(num2);
            else
            {
                Console.WriteLine("Wrong Input. Please re-Enter the number");
                Console.ReadLine();
                m = 0;
                Environment.Exit(0);
            }
            Console.ReadLine();
            if (n <= 0)
            {
                if (n == 0)
                {
                    Console.WriteLine("You Entered zero.");
                    Console.WriteLine("the result is=0");
                    Environment.Exit(0);
                }
                else
                {
                    neg1 = 1;
                    binary1 = ToBinaryString(n, neg1);
                    Console.WriteLine("Binary equivalent of number 1 with sign bit=" + binary1);
                    binary1 = binary1.Substring(1);
                }
            }
            else
            {
                binary1 = ToBinaryString(n, neg1);
                Console.WriteLine("Binary equivalent of number 1 with sign bit=" + binary1);
                binary1 = binary1.Substring(1);
            }
            if (m <= 0)
            {
                if (m == 0)
                {
                    Console.WriteLine("You Entered zero.");
                    Console.WriteLine("the result is=0");
                    Environment.Exit(0);
                }
                else
                {
                    neg2 = 1;
                    binary2 = ToBinaryString(m, neg2);
                    Console.WriteLine("Binary equivalent of number 2 with sign bit=" + binary2);
                    binary2 = binary2.Substring(1);
                }
            }
            else
            {
                binary2 = ToBinaryString(m, neg2);
                Console.WriteLine("Binary equivalent of number 2 with sign bit=" + binary2);
                binary2 = binary2.Substring(1);
            }
            Console.WriteLine("Binary equivalent of number1 without sign bit=" + binary1);
            Console.WriteLine("Binary equivalent of number2 without sign bit=" + binary2);
            Console.ReadLine();
            int decicount1 = GetDeciCount(binary1);
            int decicount2 = GetDeciCount(binary2);
            Console.WriteLine("Length of digits after decimal in binary 1=" + decicount1);
            Console.WriteLine("Length of digits after decimal in binary 2=" + decicount2);
            binary1 = binary1.Replace(".", string.Empty);
            binary2 = binary2.Replace(".", string.Empty);
            int length = decicount1 + decicount2;
            String product;
            if (binary1.Length > binary2.Length)
                product = GetBinaryMultiply(binary1, binary2);
            else
                product = GetBinaryMultiply(binary2, binary1);
            product = product.TrimStart('0');
            if (neg1 == 1)
            {
                if (neg2 == 1)
                {
                    product = product.Insert(0, "0");
                    sign = 0;
                }
                else
                {
                    product = product.Insert(0, "1");
                    sign = 1;
                }
            }
            else
            {
                if (neg2 == 1)
                {
                    product = product.Insert(0, "1");
                    sign = 1;
                }
                else
                {
                    product = product.Insert(0, "0");
                    sign = 0;
                }

            }
            product = product.Insert(product.Length - length, ".");
            Console.WriteLine("The multiplication with sign bit is=" + product);
            product = product.Substring(1);
            Console.WriteLine("The multiplication without sign bit is=" + product);
            String[] final = product.ToCharArray().Select(c => c.ToString()).ToArray();
            String[] integral = new String[product.Length - length];
            String[] fraction = new String[length];
            int i = 0, x = 0;
            while (final[i] != ".")
            {
                integral[i] = final[i];
                i++;
            }
            i++;
            while (final.Length != i)
            {
                fraction[x] = final[i];
                i++;
                x++;
            }
            String integ = GetBinToDeci(integral);
            String frac = GetBinToDeciFrac(fraction);
            char type = DetermineSign(sign);
            long value1 = long.Parse(integ);
            float value2 = float.Parse(frac);
            String result = type.ToString() + (value1 + value2).ToString();
            Console.WriteLine("The result in float is=" + result);
            Console.ReadLine();
        }
    }
}
