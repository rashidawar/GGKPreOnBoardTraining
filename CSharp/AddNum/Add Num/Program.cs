﻿using System;
using System.Numerics;
namespace Add_Num
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the first number=");
            string s = Console.ReadLine();
            BigInteger n = BigInteger.Parse(s);
            Console.WriteLine("Enter the second number=");
            string u = Console.ReadLine();
            BigInteger m = BigInteger.Parse(u);
            BigInteger sum = n + m;
            Console.WriteLine("The output is=" + sum);
            Console.ReadLine();
        }

    }
}
