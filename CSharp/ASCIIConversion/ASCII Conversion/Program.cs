﻿using System;
namespace ASCII_Conversion
{
    class Program
    {
        static void Main(string[] args)
        {
            int prime_num(int num)
            {
                int k;
                k = 0;
                for (int j = 1; j <= num; j++)
                {
                    if (num % j == 0)
                        k++;
                }
                if (k == 2)
                {
                    return num;
                }
                else
                {
                    return 0;
                }
            }
            Console.WriteLine("Enter the string=");
            string s =Console.ReadLine();
            int length = s.Length;
            int i=0;
            int[] arr = new int[length];
            foreach (char c in s)
            {
                arr[i]=Convert.ToInt32(c);
                i++;
            }
            int[] newarr= new int[length-1];
            for(i=0;i<length-1;i++)
            {
                newarr[i] = (arr[i] + arr[i + 1]) / 2;
                if (Convert.ToBoolean(prime_num(newarr[i])))
                    newarr[i]++;
            }
            s= new string(Array.ConvertAll(newarr, x => (char)x));
            Console.WriteLine("new string= " +s);
            Console.ReadLine();
        }
    }
}
